### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 396d0a9f-6a7e-4e33-8568-4c504b9341dc
using Pkg

# ╔═╡ 0033815a-21cf-4fe7-9ab4-8eaee63a8201
Pkg.activate("Project.toml")

# ╔═╡ e18f4176-4c28-4839-9923-8ae296791979
using ImageIO

# ╔═╡ b3456b8c-df68-4b09-be04-c69a8432aa96
using MLDatasets: MNIST


# ╔═╡ d5c69ead-e2ef-40f3-9ac3-4e6aa790a8a5
using Base.Iterators: repeated, partition

# ╔═╡ ca3835fa-69c2-4c42-8c8d-4ff811f493f5
using PlutoUI

# ╔═╡ c3b9c1de-234a-4eef-8a62-e3186d94a3fd
using DataFrames


# ╔═╡ 2df6bf6b-42ac-4da9-a1ca-47f65b9c96b6
using Images 

# ╔═╡ c75e78dd-012a-42ac-be94-77de0909e0b9
using FileIO

# ╔═╡ 2f308ff2-69ae-49dd-834f-09853ad5950d
using Flux.Data: DataLoader

# ╔═╡ 9cd26517-69f0-4dde-b13a-5288b0f10984
using MLDatasets

# ╔═╡ 21d74f20-d0e7-464a-9319-f963b51ca2ca
using Flux

# ╔═╡ ff0d12fb-6115-4acb-a23d-04168ddfe2f1
using Flux: onehotbatch, onecold, crossentropy, throttle 

# ╔═╡ ca3df779-53c3-4681-ab9c-066c16cced0c
using Plots


# ╔═╡ 2fbe9b90-cf56-11eb-3c80-3d0ba524f358

#using Pkg 

# ╔═╡ f330b8e5-40fd-4767-86de-645a1fd03630


# ╔═╡ f2f159ab-9afd-4f69-b475-698d9eb8b732


# ╔═╡ f05b4ca3-9090-42fc-b634-991ce1d053a7


# ╔═╡ 48fbd71d-64e7-4ea7-b909-5fbc7cb2ebe6
#=begin
    # here we define the train and test sets.
    batchsize = 128
    mb_idxs = partition(1:length(train_x), batchsize)
    train_set = [make_minibatches(train_x, train_lables, i) for i in mb_idxs]
    test_set = make_minibatches(test_x, test_lables, 1:length(x_test))
end=#

# ╔═╡ 4a9c0b55-3db0-431c-b419-3b96f02391b7


# ╔═╡ 233d73c8-9b12-4969-8055-c64945ca1880


# ╔═╡ 6298895a-027c-48bd-877d-cd5e16b005df


# ╔═╡ f5968a77-3b2a-4df0-9318-c2f620dc7703
md"# REading Files"

# ╔═╡ 1bc247f8-d767-43d8-9a92-7df702fe96fb
Test_normal = ("C:\\Users\\Digital\\Documents\\lee\\Semester 5\\Artificial Intelligence\\Lab4New\\chest_xray\\test\\NORMAL\\")

#reading test normal directory 

# ╔═╡ dcf720b8-c346-4e8d-9495-ebf926e04861
Test_pneumonia =("C:\\Users\\Digital\\Documents\\lee\\Semester 5\\Artificial Intelligence\\Lab4New\\chest_xray\\test\\PNEUMONIA\\")


# reading test pneumonia directory

# ╔═╡ 6f894671-befd-4c1c-8c0f-6787b486ccb4


# ╔═╡ 00f37fd7-7b91-472b-9a83-19fd64af0ed6

directory_pathP= Test_pneumonia

# ╔═╡ d32d06df-cf59-4952-8438-4d830edc2a2e
directory_pathN= Test_normal

# ╔═╡ 0ad6dea1-a351-4971-9ad0-ae499671eea9
directory_files= readdir(directory_pathP)

# ╔═╡ d573ecd6-18c8-49b4-8407-e9e1a4c3b5ee
directory_files2 =readdir(directory_pathN)

# ╔═╡ 3f3f4cfb-1c87-40ec-ab2d-eb76f407dd2c


# ╔═╡ 0c6d5815-23e2-4a0b-96dd-d562c9f5945e


# ╔═╡ 7a4e00a3-a5df-4374-b9ba-0fcd1e234682
begin 
	
TrainP= ("C:\\Users\\Digital\\Documents\\lee\\Semester 5\\Artificial Intelligence\\Lab4New\\chest_xray\\train\\PNEUMONIA\\")	
	
TrainN=	("C:\\Users\\Digital\\Documents\\lee\\Semester 5\\Artificial Intelligence\\Lab4New\\chest_xray\\train\\NORMAL\\")
	


	 
end 

# ╔═╡ 123828c1-6436-431e-97f6-741360785a38
Train_PImgs= readdir(TrainP)

# ╔═╡ b023633d-1d86-47df-861c-51a53c912bfc

Train_NImgs= readdir(TrainN)

# ╔═╡ f7e87c85-e058-4651-8c30-ec5cffb0275c
md"# Load Images "

# ╔═╡ 8d5f69c4-7aa6-4184-af5d-706dd51a07d5
function testX(file,path)
for image_name in file
	image_path = joinpath(path,image_name)
	image = load(image_path)
	image = Gray.(image)
	image = imresize(image,(80,80))
	image = vec(image)
	newImg= convert(Array{Float64,1},image)

return newImg
	end
end
 # function to to convert load images in gray and resize them 

# ╔═╡ 8082821f-2862-4486-8aa5-6b8cd4231e73
md"#  count of lables"

# ╔═╡ a726da9b-b71e-4ff7-b3aa-b7b5f586ecae
md"#### Test"

# ╔═╡ 50c40483-2735-4b49-8ccc-cb25000e3ff7
count= length(directory_files)

# ╔═╡ 65fc1f58-8422-48ad-a075-cd8fc5b2b8b6
count2= length(directory_files2)

# ╔═╡ 09251114-95f5-4e41-963b-85e008050d21
testLablecount= count+count2

# ╔═╡ 335694c5-0dba-45b5-a3a2-f7c3aef9f520
md"#### Training"

# ╔═╡ 5b7de9a4-c6e5-43a6-8090-f9fc438850fe
Train_count= length(Train_PImgs)

# ╔═╡ a609ea21-2de8-4ed0-84cf-e0f5dfc4219b
Train_count2= length(Train_NImgs)

# ╔═╡ 5837f37f-c980-4d93-9066-530f5cc7d8bf
trainlablecount=Train_count+Train_count2

# ╔═╡ 45b4f10e-fa67-447d-a12a-cdeda8e287b6
begin 
	

	N =["normal"]
	P=["Peunmonia"]
	
	
	Train_N =["normal"]
	Train_P=["Peunmonia"]
	
	normLable =repeat(N,count)
	pneumLable=repeat(P,count2)
	
	Train_normLable =repeat(Train_N,Train_count)
	Train_pneumLable=repeat(Train_P,Train_count2)
	 

end 
	

# ╔═╡ f0100da3-5718-42d8-a9e3-8c8887a57177


# ╔═╡ 1e285c99-bcf2-4edc-a5b8-ff38e34153a5


# ╔═╡ 9a095d55-813e-4530-b70f-2adcffabde42
md"# Train Data"

# ╔═╡ 452d8032-a8f0-43d9-a216-41e1498ea331
begin
Train_Pneumonia =testX(Train_PImgs,TrainP)
Train_Normal =testX(Train_NImgs,TrainN)
end 

# ╔═╡ 0d8950d8-b95d-4793-a5e2-d8946a244298
Train_Normal

# ╔═╡ 252ae6d6-ac67-431f-892e-cf4c5b3f6998
begin
train_x= vcat(Train_Normal,Train_Pneumonia)
#train_x =reshape(train_x,80,80,1,:)
end 

# ╔═╡ 4d0d8660-3dc8-4f1e-bb56-5b18bc1538e5
begin 
	#train_lables = onehotbatch(train_lables, 0:2)
	#train_lables = vcat(Train_normLable,Train_pneumLable)
end

# ╔═╡ c62a1cc8-6b83-4e23-bbcb-3f689728b5d4
train_lables = vcat(Train_normLable,Train_pneumLable)

# ╔═╡ efbe8b14-6827-44ce-bc2b-ded4e5ce4827
#train_lables = vcat(Train_normLable,Train_pneumLable)

#train_labels = vcat([0 for _ in 1:length(Train_normLable)], [1 for _ in #1:length(Train_pneumLable)])


# ╔═╡ 8398155b-dc83-4a0d-b0af-e72b1685091a
md"# Test Data"

# ╔═╡ 4c84081a-a55f-4d54-8ed0-96ad55fbf3d9

begin
Test_Pneumonia=testX(directory_files,Test_pneumonia)
Test_Normal =testX(directory_files2,Test_normal)
end 

# ╔═╡ a45647e5-1c6f-4927-82df-37046c238389
test_x= cat(Test_Pneumonia,Test_Normal,dims=(1,1))

# ╔═╡ 1e299057-b1cb-41b1-9a99-a669ec984caf
begin
test_lables = vcat(normLable,pneumLable)
	

end


# ╔═╡ 9ec0f5ec-3781-45c4-a608-a0ec5788f407


# ╔═╡ b46eb6c7-fdbc-4556-ac32-83bbb9efd1e5
md"# Bundle Data"

# ╔═╡ 3da3932e-07e3-403d-9cde-1430d71728b4


# ╔═╡ e693d70c-4b99-442e-a761-5aa077d3384f

    # here we define the train and test sets.
    batchsize = 128
    


# ╔═╡ f37f8200-6f3c-40fe-947a-51304a75f6a6
mb_idxs = partition(1:length(train_x), batchsize)

# ╔═╡ 4fcac3e6-681d-4c6d-9011-3afa70c0f304
test_set = make_minibatches(test_x, test_lables, 1:length(test_x))

# ╔═╡ b726be5c-8830-4446-8dfa-f1bb52891b4a
md"#  MODEL"

# ╔═╡ 1b565fd9-30f8-459d-8390-c97567c32b8d
model = chain (
 Chain(
        Conv((3, 3), 1=>32, pad=(1,1), relu),
        MaxPool((2,2)),
        Conv((3, 3), 32=>64, pad=(1,1), relu),
        MaxPool((2,2)),
        Conv((3, 3), 64=>128, pad=(1,1), relu),
        MaxPool((2,2)),
        flatten,
        Dense(12800, 2),
        softma

)

# ╔═╡ 62a99291-ee01-4a3f-a21a-283a7a5e1ac0


# ╔═╡ 18aa9f4b-10bf-49ea-b603-038c7aa819b1
1+1

# ╔═╡ d90ff046-b1a7-4992-a200-0b18e52f2992
TrainLableNew= vcat([0 for _ in 1:length(Train_normLable)],[1 for _ in 1:length(Train_pneumLable)])

# ╔═╡ 4899dace-7c35-4a43-b3a0-577e85998cb4
train_set = [make_minibatches(train_x, TrainLableNew, i) for i in mb_idxs]

# ╔═╡ d3add3b3-4a7f-46de-8b79-a30e4e0bd6b5


# ╔═╡ d7a78cfb-d037-4cd7-a45d-fc6987628ae4


# ╔═╡ 822a733c-bb75-486c-b103-f0d4e186cae0


# ╔═╡ a39aa41e-3106-441c-9905-493701dad673


# ╔═╡ d527395e-b49e-43c8-9fce-f5c684f841de



# ╔═╡ cd6aaaa1-5737-4627-9ce6-fd34f07851fc


# ╔═╡ b299b52c-35db-4a31-bb52-19b5d4fa7ddd


# ╔═╡ 0d7def7c-7624-42e1-b503-f2ea71d15214
md"# Describing Images "

# ╔═╡ f7dd03fb-ad95-4735-be8d-868fc6de576d


# ╔═╡ 774d7d76-2a41-4443-bd06-af565bb04f80


# ╔═╡ c5f870b3-b95a-4df8-b09e-2ced237d9992


# ╔═╡ 2d07f461-5846-4e2f-9f7b-d65f7e11a93c


# ╔═╡ eae06302-e5db-46e3-96e1-d0f7b292d5a2


# ╔═╡ 79f24500-3d55-4855-ae0b-7b20000ba1c7


# ╔═╡ 8ecfac53-ebb7-41de-97c2-2111dd53bcf8


# ╔═╡ c8e8e072-78d9-46a6-9026-37af9178d3db


# ╔═╡ b0185ca1-9f7d-454d-ad46-865933ba9185


# ╔═╡ bd503f4c-f677-4c37-a692-2798144522d7


# ╔═╡ c9238aae-0382-4d89-b058-18fba1e9b9ce


# ╔═╡ be7276e7-aa70-4f58-a1fd-21258cd7d4e6
   Tlabels = vcat([0 for _ in 1:length(TestP)], [1 for _ in 1:length(TestN)])

# ╔═╡ e46314a1-dd4d-41c3-9fab-5a7c969e021d


# ╔═╡ 700f9eb8-2516-427c-b408-24701a076253


# ╔═╡ 7855b4e1-4ddc-498a-b641-4d73d30d4e6b


# ╔═╡ Cell order:
# ╠═2fbe9b90-cf56-11eb-3c80-3d0ba524f358
# ╠═f330b8e5-40fd-4767-86de-645a1fd03630
# ╠═396d0a9f-6a7e-4e33-8568-4c504b9341dc
# ╠═e18f4176-4c28-4839-9923-8ae296791979
# ╠═0033815a-21cf-4fe7-9ab4-8eaee63a8201
# ╠═b3456b8c-df68-4b09-be04-c69a8432aa96
# ╠═d5c69ead-e2ef-40f3-9ac3-4e6aa790a8a5
# ╠═ca3835fa-69c2-4c42-8c8d-4ff811f493f5
# ╠═c3b9c1de-234a-4eef-8a62-e3186d94a3fd
# ╠═2df6bf6b-42ac-4da9-a1ca-47f65b9c96b6
# ╠═c75e78dd-012a-42ac-be94-77de0909e0b9
# ╠═2f308ff2-69ae-49dd-834f-09853ad5950d
# ╠═9cd26517-69f0-4dde-b13a-5288b0f10984
# ╠═21d74f20-d0e7-464a-9319-f963b51ca2ca
# ╠═ff0d12fb-6115-4acb-a23d-04168ddfe2f1
# ╠═ca3df779-53c3-4681-ab9c-066c16cced0c
# ╠═f2f159ab-9afd-4f69-b475-698d9eb8b732
# ╠═f05b4ca3-9090-42fc-b634-991ce1d053a7
# ╠═48fbd71d-64e7-4ea7-b909-5fbc7cb2ebe6
# ╠═4a9c0b55-3db0-431c-b419-3b96f02391b7
# ╠═233d73c8-9b12-4969-8055-c64945ca1880
# ╠═6298895a-027c-48bd-877d-cd5e16b005df
# ╠═f5968a77-3b2a-4df0-9318-c2f620dc7703
# ╠═1bc247f8-d767-43d8-9a92-7df702fe96fb
# ╠═dcf720b8-c346-4e8d-9495-ebf926e04861
# ╠═6f894671-befd-4c1c-8c0f-6787b486ccb4
# ╠═00f37fd7-7b91-472b-9a83-19fd64af0ed6
# ╠═d32d06df-cf59-4952-8438-4d830edc2a2e
# ╠═0ad6dea1-a351-4971-9ad0-ae499671eea9
# ╠═d573ecd6-18c8-49b4-8407-e9e1a4c3b5ee
# ╠═3f3f4cfb-1c87-40ec-ab2d-eb76f407dd2c
# ╠═0c6d5815-23e2-4a0b-96dd-d562c9f5945e
# ╠═7a4e00a3-a5df-4374-b9ba-0fcd1e234682
# ╠═123828c1-6436-431e-97f6-741360785a38
# ╠═b023633d-1d86-47df-861c-51a53c912bfc
# ╠═f7e87c85-e058-4651-8c30-ec5cffb0275c
# ╠═8d5f69c4-7aa6-4184-af5d-706dd51a07d5
# ╠═8082821f-2862-4486-8aa5-6b8cd4231e73
# ╠═a726da9b-b71e-4ff7-b3aa-b7b5f586ecae
# ╠═50c40483-2735-4b49-8ccc-cb25000e3ff7
# ╠═65fc1f58-8422-48ad-a075-cd8fc5b2b8b6
# ╠═09251114-95f5-4e41-963b-85e008050d21
# ╠═335694c5-0dba-45b5-a3a2-f7c3aef9f520
# ╠═5b7de9a4-c6e5-43a6-8090-f9fc438850fe
# ╠═a609ea21-2de8-4ed0-84cf-e0f5dfc4219b
# ╠═5837f37f-c980-4d93-9066-530f5cc7d8bf
# ╠═45b4f10e-fa67-447d-a12a-cdeda8e287b6
# ╠═f0100da3-5718-42d8-a9e3-8c8887a57177
# ╠═1e285c99-bcf2-4edc-a5b8-ff38e34153a5
# ╠═9a095d55-813e-4530-b70f-2adcffabde42
# ╠═452d8032-a8f0-43d9-a216-41e1498ea331
# ╠═0d8950d8-b95d-4793-a5e2-d8946a244298
# ╠═252ae6d6-ac67-431f-892e-cf4c5b3f6998
# ╠═4d0d8660-3dc8-4f1e-bb56-5b18bc1538e5
# ╠═c62a1cc8-6b83-4e23-bbcb-3f689728b5d4
# ╠═efbe8b14-6827-44ce-bc2b-ded4e5ce4827
# ╠═8398155b-dc83-4a0d-b0af-e72b1685091a
# ╠═4c84081a-a55f-4d54-8ed0-96ad55fbf3d9
# ╠═a45647e5-1c6f-4927-82df-37046c238389
# ╠═1e299057-b1cb-41b1-9a99-a669ec984caf
# ╠═9ec0f5ec-3781-45c4-a608-a0ec5788f407
# ╠═b46eb6c7-fdbc-4556-ac32-83bbb9efd1e5
# ╠═3da3932e-07e3-403d-9cde-1430d71728b4
# ╠═e693d70c-4b99-442e-a761-5aa077d3384f
# ╠═f37f8200-6f3c-40fe-947a-51304a75f6a6
# ╠═4899dace-7c35-4a43-b3a0-577e85998cb4
# ╠═4fcac3e6-681d-4c6d-9011-3afa70c0f304
# ╠═b726be5c-8830-4446-8dfa-f1bb52891b4a
# ╠═1b565fd9-30f8-459d-8390-c97567c32b8d
# ╠═62a99291-ee01-4a3f-a21a-283a7a5e1ac0
# ╠═18aa9f4b-10bf-49ea-b603-038c7aa819b1
# ╠═d90ff046-b1a7-4992-a200-0b18e52f2992
# ╠═d3add3b3-4a7f-46de-8b79-a30e4e0bd6b5
# ╠═d7a78cfb-d037-4cd7-a45d-fc6987628ae4
# ╠═822a733c-bb75-486c-b103-f0d4e186cae0
# ╠═a39aa41e-3106-441c-9905-493701dad673
# ╠═d527395e-b49e-43c8-9fce-f5c684f841de
# ╠═cd6aaaa1-5737-4627-9ce6-fd34f07851fc
# ╠═b299b52c-35db-4a31-bb52-19b5d4fa7ddd
# ╟─0d7def7c-7624-42e1-b503-f2ea71d15214
# ╠═f7dd03fb-ad95-4735-be8d-868fc6de576d
# ╠═774d7d76-2a41-4443-bd06-af565bb04f80
# ╠═c5f870b3-b95a-4df8-b09e-2ced237d9992
# ╠═2d07f461-5846-4e2f-9f7b-d65f7e11a93c
# ╠═eae06302-e5db-46e3-96e1-d0f7b292d5a2
# ╠═79f24500-3d55-4855-ae0b-7b20000ba1c7
# ╠═8ecfac53-ebb7-41de-97c2-2111dd53bcf8
# ╠═c8e8e072-78d9-46a6-9026-37af9178d3db
# ╠═b0185ca1-9f7d-454d-ad46-865933ba9185
# ╠═bd503f4c-f677-4c37-a692-2798144522d7
# ╠═c9238aae-0382-4d89-b058-18fba1e9b9ce
# ╠═be7276e7-aa70-4f58-a1fd-21258cd7d4e6
# ╠═e46314a1-dd4d-41c3-9fab-5a7c969e021d
# ╠═700f9eb8-2516-427c-b408-24701a076253
# ╠═7855b4e1-4ddc-498a-b641-4d73d30d4e6b
